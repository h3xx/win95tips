import { Component, OnInit } from '@angular/core';
import {
    WeirdMessageService,
} from '../services';

@Component({
    selector: 'app-tipoftheday',
    templateUrl: './tipoftheday.component.html',
    styleUrls: ['./tipoftheday.component.scss']
})
export class TipofthedayComponent implements OnInit {
    private projectUrl = 'https://codeberg.org/h3xx/win95tips';

    msg: string;

    constructor(
        private weirdMessageService: WeirdMessageService,
    ) { }

    ngOnInit() {
        this.msg = this.weirdMessageService.getRandomMessage();
    }

    close() {
        this.msg = 'You can\'t escape.';
    }

    whatsNew() {
        window.location.href = this.projectUrl;
    }

    onlineRegistration() {
        window.location.href = this.projectUrl;
    }

    nextTip() {
        this.msg = this.weirdMessageService.getNextMessage();
    }

}
