all: angular-prod

angular-prod:
	pnpm install --prefer-frozen-lockfile && npx ng build --prod

deploy: angular-prod
	git checkout -f pages || git checkout --orphan=pages
	git rm -rf .
	for fn in dist/win95-box/*; do rm -rf $$(basename $$fn) && mv $$fn . && git add $$(basename $$fn); done
	git commit -m 'Deploy Codeberg pages site'
	git branch --set-upstream-to="$$(git config "branch.main.remote")/pages"; true


.PHONY: angular-prod deploy
